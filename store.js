#!/usr/bin/env node //shaebang to use 
// Required Node APIs
const fs = require('fs');

// file system operations
function readStore() {
  return new Promise(function(resolve, reject) {
    fs.readFile('./dict.db', 'utf8', (err,data)=>{
      if(err) throw err;
      resolve(JSON.parse(data));
    });
  });
}

function modifyStore(data) {
  let dataModified = JSON.stringify(data,null,2);
  fs.writeFile('./dict.db',dataModified,()=>{
    console.log('store has been updated :)');
  })
}

// basic crud operations
function list() {
  readStore().then((dictData)=>{
    console.log(dictData);
  });
}

function add(key,value) {
  readStore().then((dictData)=>{
    dictData[key] = value;
    modifyStore(dictData);
  });
}

function get(key) {
  readStore().then((dictData)=>{
    console.log(dictData[key]);
  });
}

function remove(key) {
  readStore().then((dictData)=>{
    delete dictData[key];
    modifyStore(dictData);
  });
}

function clear() {
  modifyStore({});
}

// dealing with command line arguments
function handler(CL_args) {
  let args = CL_args.slice(2);
  switch (args[0]) {
    case 'list':
      list();
      break;
    case 'add':
      add(args[1],args[2]);
      break;
    case 'get':
      get(args[1]);
      break;
    case 'remove':
      remove(args[1]);
      break;
    case 'clear':
      clear();
      break;
    default:
      console.log('this operation doesn\'t exist');
    }
}

// check if store is found in file system
if (!fs.existsSync('./dict.db')) {
  fs.writeFile('./dict.db', '{}', function (err) {
    if (err) throw err;
  });
}

// running
handler(process.argv); 
